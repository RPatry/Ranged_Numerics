C++ - Ranged Numerics
=====================

Header-only library: allows you to encode range constraints for numeric variables
into the type system, for automatic verification by the compiler and/or at run time.
You'll need a C++ 11 (or higher) compiler. The code was tested for clang and gcc.

How to get started
==================

Just copy-paste ranged.hpp into your project.
The documentation can be built with Doxygen, or you can directly check out ranged.hpp,
it should be straightforward enough.

Examples
========

```c++
Ranged_int<20, 80> my_ranged_integer { 60 }; //Create a new integer guaranteed to belong to [20; 80]
my_ranged_integer.set<25>();  //Checked at compile-time, so my_ranged_integer.set<10> or my_ranged_integer.set<95>() will fail to compile
my_ranged_integer = 50;  //operator= is checked at run-time
try {
    my_ranged_integer = 597;
} catch (Not_in_range_exception) {
    std::cerr << "Not in range!";
}
my_ranged_integer.set<20>();
my_ranged_integer--;   //Its value is now 80
my_ranged_integer++;   //And back to 20
//Operators += and -= are also defined, with well-defined underflow/overflow semantics
int back_to_normal = my_ranged_integer;
std::cout << "the ranged integer is " << my_ranged_integer;  //We can print the value, no problem
Ranged_int<0, 100> bigger_range = my_ranged_integer;
//On the other hand, this does not compile: Ranged_int<30, 40> too_small_range = my_ranged_integer;
//The reason is that there is no guarantee that my_ranged_integer belongs to [30; 40] as it can take any value in [20; 80]
```

For more detailed code samples, check out main.cpp
