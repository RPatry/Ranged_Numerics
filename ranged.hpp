#ifndef RANGED_HPP
#define RANGED_HPP

#include <limits>
#include <iostream>

/// \file
/// Contains all the code for ranged integers, and std::numeric_limits specializations.

/*! Thrown if the value given to a Ranged object does not fit within the range.
  \sa Ranged::Ranged, Ranged::operator=(Numeric)
*/
struct Not_in_range_exception
{
};

/*! A wrapper over a numeric variable, which is guaranteed to belong to [lower_bound, upper_bound].
The default value for the variable is lower_bound.
Note that the main concern of this data structure is to help code correctness,
as such the code was not heavily hand-optimized (especially when it comes to arithmetic). That doesn't mean that the compiler won't generate efficient code though.
You can add/subtract directly on a Ranged object.
The wrapper implicitly converts back to the real type of the variable, which means that binary arithmetic operations come for free (and iostreams formatting, etc).
This version is not thread-safe.
Common aliases exist to make the class more pleasant to use: examples are Ranged_int, Ranged_ushort, etc.
\sa Ranged_int, Ranged_uint, Ranged_short, Ranged_ushort, Ranged_llong, Ranged_ullong
 */
template<typename Numeric, Numeric lower_bound, Numeric higher_bound>
struct Ranged
{
public:
    static_assert(lower_bound <= higher_bound, "Trying to create an empty range");

    using numeric_type = Numeric;
    using ranged_type = Ranged<Numeric, lower_bound, higher_bound>;

    /*! Construct a new object with a numeric value.
      If the initial value does not fit in the range, Not_in_range_exception is thrown.
      \param[in] num The initial numeric value to assign (default value: lower_bound)
      \sa Not_in_range_exception
    */
    explicit Ranged(const Numeric num = lower_bound) :
	ranged_numeric{num}
    {
	if (num < lower_bound || num > higher_bound)
	{
	    throw Not_in_range_exception{};
	}
    }

    /*! Assign a numeric value to this object.
      If the value does not fit in the range, Not_in_range_exception is thrown.
      \param[in] num The numeric value to assign
      \sa Not_in_range_exception
    */
    ranged_type& operator=(const Numeric num)
    {
	if (num < lower_bound || num > higher_bound)
	{
	    throw Not_in_range_exception{};
	}
	this->ranged_numeric = num;
	return *this;
    }

    /*! The default copy constructor */
    Ranged(ranged_type const& copy) noexcept = default;
    /*! The default copy assignment */
    ranged_type& operator=(ranged_type const& copy) noexcept = default;
    /*! The default move constructor */
    Ranged(ranged_type&& copy) noexcept = default;
    /*! The default move assignment */
    ranged_type& operator=(ranged_type&& copy) noexcept = default;

    /*! Copy constructor, defined for any Ranged type whose range is contained in the range of this Ranged type.
      For example, you can copy a Ranged<short, 5, 15> into a Ranged<int, 2, 30> (or in a Ranged<short, 1, 45>), but not the other way around.
      However, you might not be able copy a Ranged_int into a Ranged_short.
      Should you fail to meet this constraint of range, the code will fail to compile.
*/
    template<typename OtherNumeric, OtherNumeric low, OtherNumeric high>
    Ranged(const Ranged<OtherNumeric, low, high>& otherNumeric) noexcept :
	ranged_numeric{otherNumeric.get()}
    {
	//Prevent compiling if the ranges are not valid.
	static_assert(low >= lower_bound, "Trying to copy to a Ranged with a more restrictive lower bound");
	static_assert(high <= higher_bound, "Trying to copy to a Ranged with a more restrictive higher bound");
    }

    /*! Copy assignment, defined for any Ranged type whose range is contained in the range of this Ranged type.
      For example, you can assign a Ranged<short, 5, 15> to a Ranged<int, 2, 30> (or in a Ranged<short, 1, 45>), but not the other way around.
      However, you might not be able copy a Ranged_int into a Ranged_short.
      Should you fail to meet this constraint of range, the code will fail to compile.
    */
    template<typename OtherNumeric, OtherNumeric low, OtherNumeric high>
    ranged_type& operator=(const Ranged<OtherNumeric, low, high>& numeric_to_copy) noexcept
    {
	//Prevent compiling if the ranges are not valid.
	static_assert(low >= lower_bound, "Trying to copy to a Ranged with a more restrictive lower bound");
	static_assert(high <= higher_bound, "Trying to copy to a Ranged with a more restrictive higher bound");
	this->ranged_numeric = numeric_to_copy.get();
	return *this;
    }

    /*! Set a new numeric value via template to this object. Compile-time checked. */
    template<Numeric new_value>
    void set(void) noexcept
    {
	static_assert(new_value >= lower_bound && new_value <= higher_bound, "New value does not fit in the range");
	this->ranged_numeric = new_value;
    }

    /*! Return the value held in this object in its original type.
     */
    constexpr Numeric get(void) const noexcept
    {
	return this->ranged_numeric;
    }

    /* Return the minimal value than can be stored in this object */
    static constexpr const Numeric min(void) noexcept
    {
	return lower_bound;
    }

    /* Return the maximal value than can be stored in this object */
    static constexpr const Numeric max(void) noexcept
    {
	return higher_bound;
    }

    /*! Implicit conversion of this object to the original numeric type.
      Mainly useful for assignment and arithmetic, ex:
      \code{.cpp}
          Ranged<int, 8, 45> my_rint{30};
          int an_integer = my_rint;
          int another_integer = my_rint * 8;
          int yet_another_integer = my_rint << 3;
          std::cout << "My ranged int: " << my_rint << std::endl;
      \endcode
 */
    constexpr operator Numeric(void) const noexcept
    {
	return this->ranged_numeric;
    }

    /*! Add to the numeric value. Overflow/underflow is allowed. */
    //If the value is known at compile-time, the compiler may perform the operation itself! (My experience with gcc)
    ranged_type& operator+=(const Numeric operand) noexcept
    {
	if (operand < 0)
	{
	    return *this -= (operand * -1);
	}
	//We add a positive value, of that we're now sure.
	//First normalise the operand so that we can perform the addition in the range.
	const auto norm_operand = operand % range_len;

	//Then perform the addition.
	if ((max() - this->ranged_numeric) >= norm_operand)
	{
	    //No overflow.
	    this->ranged_numeric += norm_operand;
	}
	else
	{
	    //overflow!
	    const auto value_to_add = norm_operand - (max() - this->ranged_numeric) - (1 * (norm_operand > 0));
	    this->ranged_numeric = min() + value_to_add;
	}
	return *this;
    }

    /*! Subtract from the numeric value. Underflow/overflow is allowed. */
    ranged_type& operator-=(const Numeric operand) noexcept
    {
	if (operand < 0)
	{
	    return *this += abs(operand);
	}
	//At this point we can assume that we subtract a positive value.

	//First normalise the operand so that we can perform the subtraction in the range.
	const auto norm_operand = operand % range_len;

	if ((this->ranged_numeric - min()) >= norm_operand)
	{
	    //No underflow
	    this->ranged_numeric -= norm_operand;
	}
	else
	{
	    //Underflow!
	    const auto value_to_subtract = norm_operand - (this->ranged_numeric - min()) - ((norm_operand > 0) * 1);
	    this->ranged_numeric = max() - value_to_subtract;
	}
	return *this;
    }

    /*! The number of possible values this object can take.
      Client code needn't use this value */
    static const auto range_len = (max() - min()) + 1;

private:
    /*! The wrapped numeric value. */
    Numeric ranged_numeric{};
};

// Aliasing for common cases
/*! Alias for Ranged<int, lower_bound, upper_bound>, with the default bounds being the limits for int */
template<int lower_bound = std::numeric_limits<int>::min(), int upper_bound = std::numeric_limits<int>::max()>
using Ranged_int = Ranged<int, lower_bound, upper_bound>;

/*! Alias for Ranged<unsigned int, lower_bound, upper_bound>, with the default bounds being the limits for unsigned int */
template<unsigned int lower_bound = std::numeric_limits<unsigned int>::min(), unsigned int upper_bound = std::numeric_limits<unsigned int>::max()>
using Ranged_uint = Ranged<unsigned int, lower_bound, upper_bound>;

/*! Alias for Ranged<short, lower_bound, upper_bound>, with the default bounds being the limits for short */
template<short lower_bound = std::numeric_limits<short>::min(), short upper_bound = std::numeric_limits<short>::max()>
using Ranged_short = Ranged<short, lower_bound, upper_bound>;

/*! Alias for Ranged<unsigned short, lower_bound, upper_bound>, with the default bounds being the limits for unsigned short */
template<unsigned short lower_bound = std::numeric_limits<unsigned short>::min(), unsigned short upper_bound = std::numeric_limits<unsigned short>::max()>
using Ranged_ushort = Ranged<unsigned short, lower_bound, upper_bound>;

/*! Alias for Ranged<long long, lower_bound, upper_bound>, with the default bounds being the limits for long long */
template<long long int lower_bound = std::numeric_limits<long long>::min(), long long upper_bound = std::numeric_limits<long long int>::max()>
using Ranged_llong = Ranged<long long, lower_bound, upper_bound>;

/*! Alias for Ranged<unsigned long long, lower_bound, upper_bound>, with the default bounds being the limits for unsigned long long */
template<unsigned long long lower_bound = std::numeric_limits<unsigned long long>::min(), unsigned long long upper_bound = std::numeric_limits<unsigned long long>::max()>
using Ranged_ullong = Ranged<unsigned long long, lower_bound, upper_bound>;

/*!Common implementation class for the std::numeric_limits specializations.
  There should be no need to use this class directly in the client code. */
template<typename RangedSpecialization>
struct numeric_limits_ranged : std::numeric_limits<typename RangedSpecialization::numeric_type>
{
public:
    /*! Redefinition of the min() limit */
    static constexpr typename RangedSpecialization::numeric_type min(void) noexcept
    {
	return RangedSpecialization::min();
    }

    /*! Redefinition of the max() limit */
    static constexpr typename RangedSpecialization::numeric_type max(void) noexcept
    {
	return RangedSpecialization::max();
    }
};

namespace std {

/*! std::numeric_limits specialization for Ranged_int */
template<int min, int max>
struct numeric_limits<Ranged_int<min, max>> : numeric_limits_ranged<Ranged_int<min, max>>
{
};

/*! std::numeric_limits specialization for Ranged_uint */
template<unsigned int min, unsigned int max>
struct numeric_limits<Ranged_uint<min, max>> : numeric_limits_ranged<Ranged_uint<min, max>>
{
};

/*! std::numeric_limits specialization for Ranged_short */
template<short min, short max>
struct numeric_limits<Ranged_short<min, max>> : numeric_limits_ranged<Ranged_short<min, max>>
{
};

/*! std::numeric_limits specialization for Ranged_ushort */
template<unsigned short min, unsigned short max>
struct numeric_limits<Ranged_ushort<min, max>> : numeric_limits_ranged<Ranged_ushort<min, max>>
{
};



/*! std::numeric_limits specialization for Ranged_llong */
template<long long min, long long max>
class numeric_limits<Ranged_llong<min, max>> : numeric_limits_ranged<Ranged_llong<min, max>>
{
};

/*! std::numeric_limits specialization for Ranged_ullong */
template<unsigned long long min, unsigned long long max>
class numeric_limits<Ranged_ullong<min, max>> : numeric_limits_ranged<Ranged_ullong<min, max>>
{
};



}

#endif // RANGED_HPP
