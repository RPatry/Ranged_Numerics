#include <iostream>
#include "ranged.hpp"

using namespace std;

int main(int, char**)
{
    Ranged_int<0, 20> first_ranged{10};
    cout << first_ranged << endl;
    first_ranged = 20;
    cout << first_ranged << endl;
    first_ranged = 0;
    cout << first_ranged << endl;
    try {
	first_ranged = 21;
    }
    catch (Not_in_range_exception&)
    {
	cout << "Could not assign 21 to the ranged integer!" << endl;
    }
    try {
	first_ranged = -1;
    }
    catch (Not_in_range_exception&)
    {
	cout << "Could not assign -1 to the ranged integer!" << endl;
    }

    //Ranged_uint<-8, 39> uint_ranged{15};  //Does not compile, good.
    Ranged_uint<8, 39> uint_ranged{15};
    cout << uint_ranged << endl;
    uint_ranged = 8;
    cout << uint_ranged << endl;
    uint_ranged = 39;
    cout << uint_ranged << endl;
    uint_ranged += 1;
    cout << uint_ranged << endl;
    uint_ranged -= 1;
    cout << uint_ranged << endl;
    uint_ranged += 5;
    cout << uint_ranged << endl;
    uint_ranged += 32;
    cout << uint_ranged << endl;
    uint_ranged += 64;
    cout << uint_ranged << endl;
    uint_ranged += 65;
    cout << uint_ranged << endl;
    uint_ranged += -1;
    cout << uint_ranged << endl;

    const auto value_div = uint_ranged / 4;
    cout << value_div << endl;

    Ranged_uint<5, 40> bigger_uint_range{uint_ranged};
    cout << "bigger range: " << bigger_uint_range << endl;
    Ranged_uint<5, 40> copy_uint{bigger_uint_range};
    cout << "copy: " << copy_uint << endl;
    /*
    Ranged_uint<6, 40> this_cannot_compile{copy_uint};
    Ranged_uint<5, 39> this_neither{copy_uint};
    Ranged_uint<6, 39> are_you_crazy{copy_uint};
    */
    Ranged_ushort<10, 30> ushort_test{22};
    cout << ushort_test << endl;
    copy_uint = ushort_test;
    cout << copy_uint << endl;

    Ranged_uint<7, 7> uint_const{7};
    cout << uint_const << endl;
    uint_const = 7;
    cout << uint_const << endl;
    uint_const += 1;
    cout << uint_const << endl;
    uint_const -= 1;
    cout << uint_const << endl;
    uint_const += 938;
    cout << uint_const << endl;
    uint_const += -989;
    cout << uint_const << endl;

    const auto value_mul = uint_const * 3 * 10;
    cout << value_mul << endl;

    unsigned int val{};
    using my_range_to_input = Ranged_uint<20, 4330>;
    cout << "Input a value between " << my_range_to_input::min() << " and " << my_range_to_input::max() << endl;
    cin >> val;
    try
    {
	cout << boolalpha;
	cout << "min of my range " << numeric_limits<my_range_to_input>::min() << endl;
	cout << "max of my range " << numeric_limits<my_range_to_input>::max() << endl;
	cout << "my range: is it integer: " << numeric_limits<my_range_to_input>::is_integer << endl;
	my_range_to_input input_r{val};
	cout << "You input " << input_r << endl;
	input_r -= 1;
	cout << input_r << endl;
	input_r += 1;
	cout << input_r << endl;
	input_r -= 1;
	cout << input_r << endl;
	input_r += 1;
	cout << input_r << endl;
	input_r += 1;
	cout << input_r << endl;
	input_r -= 1;
	cout << input_r << endl;
	input_r += 1;
	cout << input_r << endl;
	input_r -= 1;
	cout << input_r << endl;
	input_r -= 1966;
	cout << input_r << endl;
	input_r += 3654;
	cout << input_r << endl;
	input_r += 8;
	cout << input_r << endl;
	input_r -= 77;
	cout << input_r << endl;
	//Surprising results up ahead, but that's what you C++ gives you when interpreting an unsigned as a negative.
	input_r += -68;
	cout << input_r << endl;
	input_r -= -37;
	cout << input_r << endl;
	//input_r.set<4331>();  //Does not compile
	//input_r.set<19>();  //Does not compile either
	input_r.set<300>();
    cout << "Constant number: " << input_r << endl;
    }
    catch (Not_in_range_exception&)
    {
	cout << "Out of range!" << endl;
    }

    return 0;
}
